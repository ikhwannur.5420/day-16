@extends('layouts.master')

@section('content')
    <h2>Show Post {{$data->id}}</h2>
    <h4>{{$data->title}}</h4>
    <p>{{$data->body}}</p>
@endsection