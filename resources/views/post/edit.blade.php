@extends('layouts.master')

@section('content')
<h2>Edit Data Id: {{$data->id}}</h2>
<form action="/post/{{$data->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control" name="title" id="title" value="{{$data->title}}" placeholder="Masukkan Title">
        @error('title')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">body</label>
        <input type="text" class="form-control" name="body" id="body" value="{{$data->body}}" placeholder="Masukkan Body">
        @error('body')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection